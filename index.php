<?php
// Tentukan domain asal dan tujuan
$domain_asal = "https://www.kominfo.go.id/";
$domain_tujuan = "https://bnn.go.id";

// Fungsi untuk melakukan pengalihan
function redirect_domain($url) {
    header("Location: $url", true, 301);
    exit();
}

// Periksa apakah pengguna mengakses domain asal
if ($_SERVER['HTTP_HOST'] == $domain_asal) {
    // Bangun URL tujuan dengan mempertahankan jalur dan parameter query
    $url_tujuan = "http://$domain_tujuan$_SERVER[REQUEST_URI]";
    // Lakukan pengalihan
    redirect_domain($url_tujuan);
} else {
    // Jika pengguna mengakses domain tujuan, biarkan saja
    echo "Selamat datang di $domain_tujuan";
}
?>